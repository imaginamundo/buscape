# Buscapé
Created to use vanilla Javascript ES6 and SASS.

Using Webpack to make ES6 compatible with current browsers and node-sass to compile CSS files.

## Requisites
* Node 9.11.1 or up;
* Git;

## Install
Clone the repo `git@gitlab.com:imaginamundo/buscape.git`.

Create a `.env` file. There is a `.env.default`, you can duplicate and rename to `.env`.

Run `yarn start` to create local server.

## Commands
`yarn test` or `npm test`: Test from folder `./test`;

`yarn start` or `npm start` to start node server;

`yarn build` or `npm run build`: Build static assets;

`yarn build:js` or `npm run build:js`: Build JS;

`yarn build:css` or `npm run build:css`: Build CSS;

`yarn watch` or `npm run watch`: Watch for changes on Sass and Javascript and generate files;

## Source
- [/js](#javascript-src)
    - [/components](#components-src)
        - component_1.js
        - component_2.js
    - [script.js](#script-file)
- [/scss](#sass-src)
    - **/*.scss

### Javascript src
Generated with webpack. Use ES6 features and ESlinter.

### Components src
Used by `script`. Components will be called by others `.js`. Components will not be minified or transpiled alone.

Can be used by `script` and even `components` itself.

### Script javascript file
Common Javascript for every page.

### Sass src
Generated with node-sass, default file organization, don't compile files that start with underscore, compile any other file into current folder.

## Distribution
- /js
    - script.js
- /css
    - \*\*/*.css
const express = require('express');

const router = express.Router();
const controller = require('../controllers');

// Home
router.get('/', controller.category);

module.exports = router;